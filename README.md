# Blackboard to PDF Notes Conversion

An application for automatic conversion of paper notes and blackboard content to PDFs using Handwriting Recognition (HWR) for detecting handwritten text and Optical Shape Detection for detecting hand-drawn diagrams, plots, mindmaps, etc.

The entire project is developed using National Instrument’s LabVIEW software. By the companion app of the project, images of blackboards clicked on any standard smartphone can be automatically converted into PDF documents that can be easily read from and shared among a large group of people.

<p align="center">
  <img src="frontpanel.png" />
</p>
  
## Installation

### Prerequisite

- [LabVIEW](https://www.ni.com/en-in/shop/labview.html) installation for the relevant OS
- A photo scanner mobile application
- A mobile-desktop file synchronization application
- NI [Vision Development Module](https://www.ni.com/en-us/shop/software/products/vision-development-module.html)
- [Exaprom PDF 2.0](https://forums.ni.com/t5/Example-Code/PDF-Report-with-ItextSharp/ta-p/3500858?profile.language=en) LabVIEW module

1. Open `Home_Driver.vi` from within LabVIEW.
2. Using the Vision Assistant module, train a character set of the handwriting to be detected. In the `Character Set` field of the Front Panel window, enter the location of the created character set.
2. Execute `Run` option on Front Panel window.
3. Click an image of any blackboard using a scanner mobile application (Microsoft Lens, etc).
4. Place the scanned image in the folder selected for the `DrobBox Link` field of the front panel.
5. Wait till the `Conversion Complete` notification is triggered by the application.
6. The exported PDF will be available at the location given by the application dialog.

## License

Distributed under the GNU General Public License v3.0 License. See `LICENSE` for more information.
